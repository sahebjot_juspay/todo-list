"use strict";

exports.getValueById = function(elemId) {
  var elem = document.getElementById(elemId);
  return elem.value;
}

exports.setValueById = function(elemId) {
  var elem = document.getElementById(elemId);
  return function(value) {
    elem.value = value;
    return function(){}
  }
}


function setElemAttrs(elem, attrs) {
    var ignoreAttrs = ["innerHTML", "parentId"];
    if(attrs.hasOwnProperty('innerHTML')) {
      elem.innerHTML = attrs.innerHTML;
    }
    Object.keys(attrs).map(function(key){
    if(ignoreAttrs.indexOf(key) == -1)
        elem.setAttribute(key, attrs[key]);
    });
}


exports.setElemAttrsById = function(elemId) {
  var elem = document.getElementById(elemId);
  return function(attrs) {
      setElemAttrs(elem, attrs);
    return function() {
    }
  }
}

exports.removeEventListener = function(elemId) {
  var elem = document.getElementById(elemId);
  return function(type) {
    elem.removeEventListener()
  }
}

exports.addEventListener = function(elemId) {
  console.log("adding event listener ", arguments)
  var elem = document.getElementById(elemId);
  return function(eventType) {
    return function(cb) {
      return function() {
        var cbHandler = function(event) {
          cb(event)();
        }
        elem.addEventListener(eventType, cbHandler);
      }
    }
  }
}

exports.createElementByAttrs = function(elemType) {
  var elem = document.createElement(elemType);
  return function(attrs) {
    setElemAttrs(elem, attrs);
    return function() {
      document.getElementById(attrs.parentId).appendChild(elem);
    }
  }
}
