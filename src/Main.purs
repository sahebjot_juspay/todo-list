module Main where

import Prelude

import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Console (CONSOLE, log, logShow)
import Control.Monad.Eff.Ref (newRef, modifyRef, readRef, REF)
import Data.Array (length, snoc, zip, (!!), (..))
import Data.Array (modifyAt)
import Data.Foldable (foldM, for_)
import Data.Int (fromString)
import Data.Maybe (Maybe(..), fromJust)
import Data.String (Pattern(..), split)
import Data.Tuple (Tuple(..))
import Partial.Unsafe (unsafePartial)
import Data.HeytingAlgebra(not)

foreign import addEventListener :: forall e eff. String -> String -> ({ | e} -> Eff (console :: CONSOLE, ref :: REF | eff) Unit) -> Eff eff Unit
foreign import getValueById :: String -> String
foreign import setValueById :: forall eff. String -> String -> Eff eff Unit
foreign import setElemAttrsById :: forall eff e. String -> { | e } -> Eff eff Unit
foreign import createElementByAttrs :: forall e eff. String -> { | e } -> Eff eff Unit

todoMenuId = "menu"
addTodoId = "addTodo"
todoInput = "todoInput"
todoListContainerId = "todo-list"

todoListRef = newRef []

type Todo = {content :: String, isChecked :: Boolean}
type TodoList = Array Todo

createTodoItem todo index = do
  let chkBoxId = "chkBox_" <> show index
      chkBox = "<input id=\"" <> chkBoxId <> "\" type=\"checkbox\"/>"
      todoSpan = "<span>" <> todo.content <> "</span>"
      todoContent = if todo.isChecked then "<strike>" <> todoSpan <> "</strike>" else todoSpan
  logShow todoContent
  createElementByAttrs "div" {parentId: todoListContainerId, innerHTML: chkBox <> todoContent}


updateTodoItem event todoRef = do
  let arrS = (split (Pattern "_") event.target.id)
      elemIndex = unsafePartial $ fromJust $ fromString $ unsafePartial $ fromJust (arrS !! 1)
  _ <- modifyRef todoRef (\todoList -> case modifyAt elemIndex (\{content:c,isChecked:b} -> {content: c, isChecked: not b :: Boolean}) todoList of
                          Just arr -> arr
                          Nothing  -> [])
  todoList <- readRef todoRef
  drawTodoList todoRef


drawTodoList todoRef = do
  todoList <- readRef todoRef
  -- clear todolist
  _ <- setElemAttrsById todoListContainerId {innerHTML: ""}
  -- redraw the todolist
  let todoListWithIndex = zip todoList (0..(length todoList))
  for_ todoListWithIndex \(Tuple todo index) -> do
     createTodoItem todo index

addTodo todoRef = do
  let todoVal = getValueById todoInput
  _ <- modifyRef todoRef (\todoList -> snoc todoList {content: todoVal, isChecked: false})
  setValueById todoInput ""
  drawTodoList todoRef
  todoList <- readRef todoRef
  pure todoRef

addChangeListener todoRef = do
  todoList <- readRef todoRef
  for_ (0..(length todoList - 1)) \todoIndex -> do
     addEventListener ("chkBox_" <> show todoIndex) "change" (\e -> updateTodoItem e todoRef)


--main :: forall e. Eff (console :: CONSOLE | e) Unit
main = do
  currRef <- todoListRef
  _ <- createElementByAttrs "input" {id: todoInput, placeholder: "todo", parentId: todoMenuId}
  _ <- createElementByAttrs "button" {id: addTodoId, parentId: todoMenuId, innerHTML: "Add Todo"}
  addEventListener addTodoId "click" (\event -> do
                                           todoRef <- addTodo currRef
                                           addChangeListener todoRef
                                           pure unit
                                        )
